// Pin Connections:
// Vcc - 5v
// GND - GND
// SDA/SCL - Connect above AREF pin

#include <Wire.h>
#include <LiquidCrystal_I2C.h>

//Addr: 0x3F, 20 chars & 4 lines
LiquidCrystal_I2C lcd(0x3F, 20, 4); 

void setup()
{
  lcd.init(); 
  lcd.backlight();
  lcd.setCursor(0, 0);
  lcd.print("Hello");
  lcd.setCursor(0, 1);
  lcd.print("World");
  lcd.setCursor(0, 2);
  lcd.print("This is ARDUINO");
  lcd.setCursor(0, 3);
  for(int i = 0; i < 20; i++)
  {
    lcd.setCursor(i,3);
    lcd.print("-");
  }
  
  for(int i = 0; i < 20; i++)
  {
    lcd.setCursor(i,3);
    lcd.print("-");
  } 
}
void loop()
{

}


