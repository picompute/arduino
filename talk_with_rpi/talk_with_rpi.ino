#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <dht.h>

#define DHT22_PIN 6
#define led 13

//Addr: 0x3F, 20 chars & 4 lines
LiquidCrystal_I2C lcd(0x3F, 20, 4); 
dht DHT;

// --------------
// Setup
// --------------
void setup()
{
  lcd.init(); 
  lcd.backlight();
  printLine(0, "ARDUINO-PI via USB");
  printLine(1, "------------------");
  printLine(2, "Reading: RaspberryPI");

  pinMode(led, OUTPUT);

  Serial.begin(9600);

  delay(2000);
  cls();
  printLine(3, "Ready...");
}

// --------------
// LOOP
// --------------
void loop()
{
  if (Serial.available())  {
    String s = Serial.readStringUntil('\n');
    perform(s);
  }
  delay(500);
}

void perform(String input)
{
  String fn = getValue(input, ':', 0);
  String args = getValue(input, ':', 1);

  cls(); 

  if(fn.equals("morse"))
  { 
    printLine(0, "Morse Signal:");

    char w[args.length() + 1];
    args.toCharArray(w, args.length()+1);

    int row = 1;
    int col = -1;
    int len = strlen(w);
    for (int i = 0; i < len; i++)
    {
      col = col + 1;
      if (col > 19) { 
        col = 0; 
        row = row +1; 
        if (row > 3) {
          row = 1;
          clearLine(1);
          clearLine(2);
          clearLine(3);
        }
      }
      lcd.setCursor(col, row);
      lcd.print(w[i]);

      show_letter(morse_for(w[i]));
    }

    printLine(3, "DONE!");
  }
  else if(fn.equals("dht22"))
  {
    printLine(0, "Reading Temp...");
    delay(500);

    // READ DATA
    int chk = DHT.read22(DHT22_PIN);
    switch (chk)
    {
    case DHTLIB_OK:  
      printLine(1, "OK");
      printLine(2, "Humidity:    " + String((int)DHT.humidity, DEC) + "%");
      printLine(3, "Temperature: " + String((int)DHT.temperature, DEC) + " C");
      break;
    case DHTLIB_ERROR_CHECKSUM: 
      printLine(1, "Checksum error"); 
      break;
    case DHTLIB_ERROR_TIMEOUT: 
      printLine(1, "Time out error"); 
      break;
    default: 
      printLine(1, "Unknown error"); 
      break;
    }
  }
  else
  {
    printLine(0, "-- Unknown CMD");
  }

  delay(2000);  
}


// --------------
// Util functions
// --------------

void printLine(int line, String s)
{
  clearLine(line);
  lcd.setCursor(0, line);
  lcd.print(s);
}

void cls()
{
  for(int i = 0; i< 4; i++)
  {
    clearLine(i);
  }
}

void clearLine(int line)
{
  lcd.setCursor(0, line);
  lcd.print("                    ");
}

String getValue(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = {
    0, -1                  };
  int maxIndex = data.length()-1;
  for(int i=0; i<=maxIndex && found<=index; i++){
    if(data.charAt(i)==separator || i==maxIndex){
      found++;
      strIndex[0] = strIndex[1]+1;
      strIndex[1] = (i == maxIndex) ? i+1 : i;
    }
  }
  return found>index ? data.substring(strIndex[0], strIndex[1]) : "";
}

// =================
// MORSE module
// =================

int UNIT = 50;

int DASH_WIDTH = 3 * UNIT;
int DOT_WIDTH  = 1 * UNIT;
int SPACE_BETWEEN_PARTS = 1 * UNIT;

int SPACE_BETWEEN_LETTERS = 3 * UNIT;
int SPACE_BETWEEN_WORDS   = 7 * UNIT;

const char* MORSE_CODE_LETTERS[26] = {
  /* A */  ".-",
  /* B */  "-...",
  /* C */  "-.-.",
  /* D */  "-..",
  /* E */  ".",
  /* F */  "..-.",
  /* G */  "--.",
  /* H */  "....",
  /* I */  "..",
  /* J */  ".---",
  /* K */  "-.-",
  /* L */  ".-..",
  /* M */  "--",
  /* N */  "-.",
  /* O */  "---",
  /* P */  ".--.",
  /* Q */  "--.-",
  /* R */  ".-.",
  /* S */  "...",
  /* T */  "-",
  /* U */  "..-",
  /* V */  "...-",
  /* W */  ".--",
  /* X */  "-..-",
  /* Y */  "-.--",
  /* Z */  "--.."
};

const char* MORSE_CODE_NUMBERS[10] = {
  /* 0 */  "-----",  
  /* 1 */  ".----",
  /* 2 */  "..---",
  /* 3 */  "...--",
  /* 4 */  "....-",
  /* 5 */  ".....",
  /* 6 */  "-....",
  /* 7 */  "--...",
  /* 8 */  "---..",
  /* 9 */  "----."
};

const char* morse_for(const char c) { 
  if (c >= 'A' && c <= 'Z')
    return MORSE_CODE_LETTERS[c - 'A'];
  else if (c >= '0' && c <= '9')
    return MORSE_CODE_NUMBERS[c - 'A'];

  // Error condition
  return MORSE_CODE_NUMBERS[0];
};

void space(int space_width) {
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(space_width);
}

void dash() {
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(DASH_WIDTH);
}

void dot() {
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(DOT_WIDTH);
}

void show_letter(const char* morse)
{ 
  int len = strlen(morse);
  for (int i = 0; i< len; i++)
  {
    morse[i] == '.' ? dot() : dash();

    if (i < len - 1)
    {
      space(SPACE_BETWEEN_PARTS);       
    }
  }
}

void show_word(const char* w)
{
  int len = strlen(w);
  for (int i = 0; i< len; i++)
  {
    show_letter(morse_for(w[i]));

    if (i < len - 1)
    {
      space(SPACE_BETWEEN_LETTERS);       
    }
  }

  space(SPACE_BETWEEN_WORDS);
}





