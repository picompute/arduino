#include <NewPing.h>

#define SENSOR_TRIG_PIN 4
#define SENSOR_ECHO_PIN 7
#define SENSOR_MAX_DIST 200

#define MOTOR_A_DIR_PIN 12
#define MOTOR_A_PWM_PIN 03
#define MOTOR_A_BRAKE_PIN 9

#define FORWARD LOW
#define BACKWARD HIGH

#define HI_SPEED 200
#define LO_SPEED 150

#define BRAKING_DISTANCE 75
#define STOPPING_DISTANCE 30

#define DISTANCE_WARNING_PIN 2

int running = 0;

NewPing sonar(SENSOR_TRIG_PIN, SENSOR_ECHO_PIN, SENSOR_MAX_DIST);

void setup() {
  Serial.begin(115200);

  //Setup Channel A
  pinMode(MOTOR_A_DIR_PIN, OUTPUT); //Initiates Motor Channel A pin
  pinMode(MOTOR_A_BRAKE_PIN, OUTPUT); //Initiates Brake Channel A pin
  pinMode(MOTOR_A_PWM_PIN, OUTPUT); //Initiates PWM Channel A pin
  
  pinMode(DISTANCE_WARNING_PIN, OUTPUT); // Init RED Led output pin
}


void move_motors(int motor_speed, bool motor_dir) {
  digitalWrite(MOTOR_A_DIR_PIN, motor_dir); //Establishes forward direction of Channel A
  digitalWrite(MOTOR_A_BRAKE_PIN, LOW);   //Disengage the Brake for Channel A
  analogWrite(MOTOR_A_PWM_PIN, motor_speed);   //Spins the motor on Channel A
}

void brake() {
  Serial.println("Braking...");
  digitalWrite(MOTOR_A_BRAKE_PIN, HIGH); //Engage the Brake for Channel A 
}

void fwd(int motor_speed)
{
  Serial.println("Going forward...");
  move_motors(motor_speed, FORWARD);
}

void rev(int motor_speed)
{
  Serial.println("Going backward...");
  move_motors(motor_speed, BACKWARD);
}

void brake_light(bool on)
{
  digitalWrite(DISTANCE_WARNING_PIN, on ? HIGH : LOW);
}

int distance()
{
  unsigned int ping_us = sonar.ping_median(5);
  int ping_distance_cm = sonar.convert_cm(ping_us);
  Serial.println(ping_distance_cm);
  return ping_distance_cm;
}

void loop() {
  int ping_distance_cm = distance();

  if (ping_distance_cm < STOPPING_DISTANCE && ping_distance_cm != 0) {
    brake_light(true);
    rev(LO_SPEED);
    
  }
  else {
    brake_light(false);
    fwd(LO_SPEED);
  }
}

