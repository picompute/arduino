#include <NewPing.h>

#define SENSOR_TRIG_PIN 4
#define SENSOR_ECHO_PIN 7
#define SENSOR_MAX_DIST 256

#define MOTOR_A_DIR_PIN 12
#define MOTOR_B_DIR_PIN 13

#define MOTOR_A_PWM_PIN 03
#define MOTOR_B_PWM_PIN 11

#define MOTOR_A_BRAKE_PIN 9
#define MOTOR_B_BRAKE_PIN 8

#define FORWARD HIGH
#define BACKWARD LOW

#define MAX_SPEED 150
#define MID_SPEED 100
#define MIN_SPEED 50
#define OFF_SPEED 0

#define STOP 2
#define BREAK 1
#define RUN 0

#define BRAKING_DISTANCE_CM 60
#define STOPPING_DISTANCE_CM 30

#define BRAKE_LIGHT_PIN 2

#define CURRENT_SENSING_PIN A0
float VOLT_PER_AMP = 1.65;

NewPing sonar(SENSOR_TRIG_PIN, SENSOR_ECHO_PIN, SENSOR_MAX_DIST);

void setup() {
  Serial.begin(115200);

  //Setup Channel A
  pinMode(MOTOR_A_DIR_PIN, OUTPUT); //Initiates Motor Channel A pin
  pinMode(MOTOR_A_BRAKE_PIN, OUTPUT); //Initiates Brake Channel A pin
  pinMode(MOTOR_A_PWM_PIN, OUTPUT); //Initiates PWM Channel A pin

  //Setup Channel B
  pinMode(MOTOR_B_DIR_PIN, OUTPUT); //Initiates Motor Channel B pin
  pinMode(MOTOR_B_BRAKE_PIN, OUTPUT); //Initiates Brake Channel B pin
  pinMode(MOTOR_B_PWM_PIN, OUTPUT); //Initiates PWM Channel B pin

  pinMode(BRAKE_LIGHT_PIN, OUTPUT); // Init RED Led output pin
}


void move_motors(int l_speed, bool l_dir, int r_speed, bool r_dir) {
  brake_light(false);

  digitalWrite(MOTOR_A_DIR_PIN, l_dir);  //Establishes direction of Channel A
  digitalWrite(MOTOR_B_DIR_PIN, r_dir);  //Establishes direction of Channel B

  analogWrite(MOTOR_A_PWM_PIN, l_speed); //Set speed for the motor on Channel A
  analogWrite(MOTOR_B_PWM_PIN, r_speed); //Set speed for the motor on Channel B

  digitalWrite(MOTOR_A_BRAKE_PIN, LOW);  //Disengage the Brake for Channel A
  digitalWrite(MOTOR_B_BRAKE_PIN, LOW);  //Disengage the Brake for Channel B
}

void brake() {
  Serial.println("Braking...");
  brake_light(true);
  digitalWrite(MOTOR_A_BRAKE_PIN, HIGH); //Engage the Brake for Channel A
  digitalWrite(MOTOR_B_BRAKE_PIN, HIGH); //Engage the Brake for Channel B

  current_sense();
}

void brake_light(bool on)
{
  digitalWrite(BRAKE_LIGHT_PIN, on ? HIGH : LOW);
  delay(500);
}

void fwd(int motor_speed)
{
  Serial.println("Going forward...");
  move_motors(motor_speed, FORWARD, motor_speed, FORWARD);
  delay(50);

  current_sense();
}

void rev(int motor_speed)
{
  Serial.println("Going backward...");
  move_motors(motor_speed, BACKWARD, motor_speed, BACKWARD);
  delay(50);

  current_sense();
}

//void left()
//{
//  Serial.println("Going left...");
//  move_motors(OFF_SPEED, FORWARD, MID_SPEED, BACKWARD);
//}
//
//void right()
//{
//  Serial.println("Going right...");
//  move_motors(MID_SPEED, BACKWARD, OFF_SPEED, FORWARD);
//}


void current_sense()
{
  /*
  Based on code from:
   
   http://www.dustynrobots.com/news/digital-signal-processing-and-filtering-motor-current-sensing/
   */
  //  float currentRaw = analogRead(CURRENT_SENSING_PIN);
  //  float currentVolts = currentRaw * (5.0/1024.0);
  //  float currentAmps = currentVolts / VOLT_PER_AMP;
  //
  //  Serial.println(currentAmps);
}

int distance_to_action()
{
  int ping_distance_cm = sonar.convert_cm(sonar.ping_median());
  Serial.println(ping_distance_cm);

  if (ping_distance_cm > BRAKING_DISTANCE_CM)
  {
    return RUN;
  }
  else if(ping_distance_cm > STOPPING_DISTANCE_CM)
  { 
    return BREAK;
  }
  return STOP;
}

void loop() {
  int action = distance_to_action();

  switch(action)
  {
  case RUN:
    fwd(MAX_SPEED);
    break;

  case BREAK:
    fwd(MID_SPEED);
    break;

  case STOP:
    brake();

    rev(MID_SPEED);
    delay(1000);
    
    break;
  }

  delay(100);
}










