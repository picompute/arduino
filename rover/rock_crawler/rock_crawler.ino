#include <NewPing.h>

#define SENSOR_TRIG_PIN 4
#define SENSOR_ECHO_PIN 7
#define SENSOR_MAX_DIST 200

#define DRIVE_MOTOR_DIR_PIN 12
#define DRIVE_MOTOR_PWM_PIN 03
#define DRIVE_MOTOR_BRAKE_PIN 9

#define STEERING_MOTOR_DIR_PIN 13
#define STEERING_MOTOR_PWM_PIN 11
#define STEERING_MOTOR_BRAKE_PIN 8

#define FORWARD LOW
#define BACKWARD HIGH

#define LEFT HIGH
#define RIGHT LOW

#define MAX_VAL 255
#define HI_SPEED 200
#define LO_SPEED 150

#define BRAKING_DISTANCE 75
#define STOPPING_DISTANCE 30

#define BRAKE_LIGHT_PIN 2

int running = 0;

NewPing sonar(SENSOR_TRIG_PIN, SENSOR_ECHO_PIN, SENSOR_MAX_DIST);

void setup() {
  Serial.begin(115200);

  //Setup Channel A: Drive - Need speed control hence PWM used
  pinMode(DRIVE_MOTOR_DIR_PIN, OUTPUT);
  pinMode(DRIVE_MOTOR_BRAKE_PIN, OUTPUT);
  pinMode(DRIVE_MOTOR_PWM_PIN, OUTPUT);

  //Setup Channel B: Steering (NO PWM)
  pinMode(STEERING_MOTOR_DIR_PIN, OUTPUT);
  pinMode(STEERING_MOTOR_BRAKE_PIN, OUTPUT);

  pinMode(BRAKE_LIGHT_PIN, OUTPUT); // Init RED Led output pin
}

void move_motors(int motor_speed, bool motor_dir) {
  brake_light(false);
  digitalWrite(DRIVE_MOTOR_DIR_PIN, motor_dir); //Establishes forward direction of Channel A
  digitalWrite(DRIVE_MOTOR_BRAKE_PIN, LOW);   //Disengage the Brake for Channel A
  analogWrite(DRIVE_MOTOR_PWM_PIN, motor_speed);   //Spins the motor on Channel A
}

void brake() {
  Serial.println("Braking...");
  brake_light(true);
  digitalWrite(STEERING_MOTOR_BRAKE_PIN, HIGH);
  digitalWrite(DRIVE_MOTOR_BRAKE_PIN, HIGH);
}

void fwd(int motor_speed)
{
  Serial.println("Going forward...");
  move_motors(motor_speed, FORWARD);
}

void rev(int motor_speed)
{
  Serial.println("Going backward...");
  move_motors(motor_speed, BACKWARD);
}

void turn(int motor_dir)
{
  digitalWrite(STEERING_MOTOR_DIR_PIN, motor_dir);
  digitalWrite(STEERING_MOTOR_PWM_PIN, MAX_VAL);
  digitalWrite(STEERING_MOTOR_BRAKE_PIN, LOW);   

  digitalWrite(DRIVE_MOTOR_DIR_PIN, BACKWARD); 
  digitalWrite(DRIVE_MOTOR_BRAKE_PIN, LOW);  
  analogWrite(DRIVE_MOTOR_PWM_PIN, LO_SPEED);
}

void brake_light(bool on)
{
  digitalWrite(BRAKE_LIGHT_PIN, on ? HIGH : LOW);
}

int distance()
{
  unsigned int ping_us = sonar.ping_median(5);
  int ping_distance_cm = sonar.convert_cm(ping_us);
  Serial.println(ping_distance_cm);
  return ping_distance_cm;
}

void loop() {
  int ping_distance_cm = distance();

  if (ping_distance_cm < STOPPING_DISTANCE && ping_distance_cm != 0) {
    brake();
    turn(LEFT);
    delay(1000); 
    brake();   
  }
  else {
    fwd(LO_SPEED);
  }
}


